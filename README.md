# Instrucciones
Con los datos que se encuentran en los archivos ```brandDateData.json``` y ```assignedStore.json```, deberás realizar los siguientes retos:

### Reto 1
Mostrar en una tabla las siguientes propiedades:
  1. title
  2. peasants
  3. visitors
  4. attraction (visitors / peasants)
  5. cabinet
  6. tickets
  7. persuasion (tickets / visitors)
  8. revenue
  9. averageTicket (revenue / tickets)
  10. items
  11. itemperTicket ((items / tickets) / 100)
  12. averagePermanence (((permanence * 100) / permanenceCount) / 6000000)

### Reto 2
Agregar un checkbox para poder cambiar los valores de la tabla entre datos absolutos y datos promediados por día, para ello,  te recomendamos revisar la estructura  de los archivos ```brandDateData.json``` y ```assignedStore.json```, nota que los datos se encuentran por dia.

### Extra 1
Agregar columna del número de días apagados, son aquellos donde el uptime se encuentra en cero

### Extra 2
Ocultar automáticamente las columnas en donde los valores de los datos sea cero en todas las tiendas.

### Extra 3
Calcular la fila de totales e incluir la formulación matemática para las mismas, como parte de la documentación

> Te dejamos una imagen con la tabla que deberás obtener
![GitHub Logo](/dataTable.png)


## Notas
  * Podrás realizar el ejercicio con el framework de JS que decidas, o  si prefieres puedes hacerlo únicamente con JS
  * Si tienes alguna duda del ejercicio, comunícate con quien te realice la prueba
  * En cuanto finalices, envía el link de tu repositorio a la persona que te realice la prueba
